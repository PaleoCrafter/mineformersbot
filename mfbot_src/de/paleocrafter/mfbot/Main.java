package de.paleocrafter.mfbot;

import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.configuration.ConfigurationException;

import de.paleocrafter.mfbot.api.commands.PrivateCommands;
import de.paleocrafter.mfbot.api.commands.PublicCommands;
import de.paleocrafter.mfbot.api.configuration.BotConfiguration;
import de.paleocrafter.mfbot.api.configuration.Config;
import de.paleocrafter.mfbot.commands.priv.AdminCommand;
import de.paleocrafter.mfbot.commands.priv.LinksCommand;
import de.paleocrafter.mfbot.commands.pub.GoogleCommand;
import de.paleocrafter.mfbot.commands.pub.ListMailsCommand;
import de.paleocrafter.mfbot.commands.pub.LookupLinkCommand;
import de.paleocrafter.mfbot.commands.pub.MailCommand;
import de.paleocrafter.mfbot.commands.pub.SaRCommand;
import de.paleocrafter.mfbot.commands.pub.TimeCommand;
import de.paleocrafter.mfbot.api.data.Links;
import de.paleocrafter.mfbot.api.data.Mails;
import de.paleocrafter.mfbot.api.listeners.BotListeners;
import de.paleocrafter.mfbot.gui.MainFrame;
import de.paleocrafter.mfbot.listeners.CommandListener;
import de.paleocrafter.mfbot.listeners.LogListener;

/**
 * 
 * MineFormersBot
 * 
 * Main
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    BotConfiguration config;
                    if (new File("config.xml").exists())
                        config = new BotConfiguration("config.xml");
                    else
                        config = new BotConfiguration();
                    boolean success = config.init();

                    if (!success) {
                        if (!config.containsKey("name"))
                            config.setProperty("name", "ChangeMe");
                        if (!config.containsKey("channels")
                                || !config.getKeys("channels").hasNext())
                            config.setProperty("channels.channel", "ChangeMe");
                        if (!config.containsKey("shouldIdentify"))
                            config.setProperty("shouldIdentify",
                                    Config.shouldIdentify);
                        if (!config.containsKey("password")) {
                            config.setProperty("password",
                                    "Change me only if shouldIdentify is true");
                        }
                        if (!config.containsKey("server"))
                            config.setProperty("server", "ChangeMe");
                        if (!config.containsKey("port"))
                            config.setProperty("port", 6667);
                        config.save(new File("config.xml"));
                        System.out
                                .println("Press ENTER to stop the application...");
                        System.in.read();
                    } else {
                        Links.init();
                        Mails.init();
                        PublicCommands.registerCommand(new TimeCommand());
                        PublicCommands.registerCommand(new SaRCommand());
                        PublicCommands.registerCommand(new LookupLinkCommand());
                        PublicCommands.registerCommand(new MailCommand());
                        PublicCommands.registerCommand(new ListMailsCommand());
                        PublicCommands.registerCommand(new GoogleCommand());

                        PrivateCommands.registerCommand(new AdminCommand());
                        PrivateCommands.registerCommand(new LinksCommand());

                        BotListeners.registerListener(new CommandListener());
                        BotListeners.registerListener(new LogListener());

                        BotPlugins.init();
                        File pluginFolder = new File("plugins");
                        if (!pluginFolder.exists())
                            pluginFolder.mkdir();
                        BotPlugins.load(pluginFolder.getAbsolutePath());

                        UIManager.setLookAndFeel(UIManager
                                .getSystemLookAndFeelClassName());
                        MainFrame frame = new MainFrame();
                        frame.setVisible(true);
                    }
                } catch (ConfigurationException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException | InstantiationException
                        | IllegalAccessException
                        | UnsupportedLookAndFeelException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

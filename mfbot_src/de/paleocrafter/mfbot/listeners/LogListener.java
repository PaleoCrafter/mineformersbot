package de.paleocrafter.mfbot.listeners;

import java.io.IOException;

import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.exception.IrcException;
import org.pircbotx.exception.NickAlreadyInUseException;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.DisconnectEvent;
import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.events.KickEvent;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.OpEvent;
import org.pircbotx.hooks.events.QuitEvent;

import de.paleocrafter.mfbot.api.configuration.Config;
import de.paleocrafter.mfbot.api.util.BotLogger;

/**
 * 
 * MineFormersBot
 * 
 * LogListener
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
@SuppressWarnings("rawtypes")
public class LogListener extends ListenerAdapter {
    public static boolean firstConnection;

    @Override
    public void onMessage(MessageEvent event) {
        User user = event.getUser();
        String message = event.getMessage();

        // Log the sent message to file + console
        BotLogger.log(event.getChannel(),
                user.getNick() + "> " + event.getMessage());
        // Put the sent message into the log
        BotLogger.addToInternalLog(user, System.currentTimeMillis(), message);
    }

    @Override
    public void onJoin(JoinEvent event) {
        BotLogger.log(event.getChannel(), event.getUser().getNick() + " ("
                + event.getUser().getLogin() + " @ "
                + event.getUser().getHostmask() + ") has joined.");
        if (!firstConnection)
            firstConnection = true;
    }

    @Override
    public void onKick(KickEvent event) {
        BotLogger.log(event.getChannel(), event.getSource().getNick()
                + " kicked " + event.getRecipient().getNick()
                + " with reason: " + event.getReason());
        if (event.getRecipient().getNick()
                .equalsIgnoreCase(event.getBot().getNick())) {
            event.getBot().joinChannel(event.getChannel().getName());
        }
    }

    @Override
    public void onOp(OpEvent event) {
        BotLogger.log(event.getChannel(), event.getSource().getNick()
                + " oped " + event.getRecipient().getNick());
    }

    @Override
    public void onQuit(QuitEvent event) {
        for (Channel ch : event.getUser().getChannels()) {
            BotLogger.log(ch,
                    event.getUser().getNick() + " has quit (" + event.getReason()
                            + ")");
        }
    }

    @Override
    public void onDisconnect(DisconnectEvent event) {
        if (firstConnection) {
            BotLogger.log(null, "Got disconnected. Reconnecting...");
            performReconnect(event.getBot());
        }
    }

    public void performReconnect(PircBotX bot) {
        while (!bot.isConnected()) {
            try {
                bot.reconnect();
                if (Config.shouldIdentify)
                    bot.identify(Config.password);
                for (String channel : Config.channels)
                    bot.joinChannel(channel);
            } catch (NickAlreadyInUseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IrcException e) {
                e.printStackTrace();
            }
        }
    }
}

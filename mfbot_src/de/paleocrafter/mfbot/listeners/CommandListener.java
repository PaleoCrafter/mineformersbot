package de.paleocrafter.mfbot.listeners;

import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;

import de.paleocrafter.mfbot.api.commands.BotCommand;
import de.paleocrafter.mfbot.api.commands.PrivateCommands;
import de.paleocrafter.mfbot.api.commands.PublicCommands;

/**
 * 
 * MineFormersBot
 * 
 * CommandListener
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
@SuppressWarnings("rawtypes")
public class CommandListener extends ListenerAdapter {

    @Override
    public void onMessage(MessageEvent event) {
        BotCommand cmd = PublicCommands.getCommand(event.getMessage());
        if (cmd != null) {
            cmd.execute(
                    event.getBot(),
                    event.getChannel(),
                    event.getUser(),
                    event.getMessage().substring(
                            cmd.getCommandString().length()));
        }
    }

    @Override
    public void onPrivateMessage(PrivateMessageEvent event) {
        if (event.getUser().getChannelsOpIn().size() > 0) {
            BotCommand cmd = PrivateCommands.getCommand(event.getMessage());
            if (cmd != null) {
                cmd.execute(event.getBot(), null, event.getUser(), event
                        .getMessage()
                        .substring(cmd.getCommandString().length()));
            }
        }
    }
}

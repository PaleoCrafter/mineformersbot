package de.paleocrafter.mfbot.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * 
 * MineFormersBot
 * 
 * LoggerFormat
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class LoggerFormat extends Formatter {

    @Override
    public String format(LogRecord rec) {
        StringBuilder builder = new StringBuilder();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-YY HH:mm:ss");
        String date = format.format(new Date(rec.getMillis()));
        builder.append("[" + date + "] ");
        builder.append(rec.getMessage());
        builder.append("\n");
        return builder.toString();
    }
}

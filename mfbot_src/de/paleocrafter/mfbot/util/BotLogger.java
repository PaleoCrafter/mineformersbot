package de.paleocrafter.mfbot.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

import org.pircbotx.Channel;
import org.pircbotx.User;

import com.google.common.collect.HashBasedTable;

import de.paleocrafter.mfbot.api.util.LoggerFormat;
import de.paleocrafter.mfbot.gui.MainFrame;

/**
 * 
 * MineFormersBot
 * 
 * BotLogger
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class BotLogger {
    private static MainFrame frame;
    private static Logger logger;
    private static Handler fileHandler;
    public static HashBasedTable<User, Long, String> internalLog;

    public static void init(String name, MainFrame mainFrame) {
        frame = mainFrame;
        try {
            logger = Logger.getLogger(name);
            logger.setUseParentHandlers(false);
            String fileName;
            SimpleDateFormat format = new SimpleDateFormat(
                    "dd-MM-YYYY_HH-mm-ss");
            File folder = new File("logs/");
            folder.mkdir();
            fileName = folder.getAbsolutePath() + "/"
                    + format.format(new Date(System.currentTimeMillis()))
                    + ".log";

            fileHandler = new FileHandler(fileName);

            Handler consoleHandler = new ConsoleHandler();
            logger.addHandler(consoleHandler);
            logger.addHandler(fileHandler);

            LoggerFormat logFormat = new LoggerFormat();
            consoleHandler.setFormatter(logFormat);
            fileHandler.setFormatter(logFormat);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        internalLog = HashBasedTable.create();
    }

    public static void log(Channel channel, String message) {
        logger.info(message);
        if (channel != null)
            frame.addToChannelLog(channel.getName(), message);
        else
            frame.addToChannelLog(null, message);
    }

    public static void close() {
        fileHandler.close();
    }

    public static void addToInternalLog(User user, long time, String message) {
        internalLog.put(user, time, message);
        if (internalLog.size() >= 100) {
            // Get the oldest message and delete it
            Set<Long> set = internalLog.columnKeySet();
            long oldestMessage = Collections.min(set);
            User toRemove = internalLog.column(oldestMessage).keySet()
                    .iterator().next();
            internalLog.remove(toRemove, oldestMessage);
        }
    }
}

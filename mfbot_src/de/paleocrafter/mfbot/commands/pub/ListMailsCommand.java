package de.paleocrafter.mfbot.commands.pub;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;

import de.paleocrafter.mfbot.api.commands.BotCommand;
import de.paleocrafter.mfbot.api.data.Mail;
import de.paleocrafter.mfbot.api.data.Mails;

/**
 * 
 * MineFormersBot
 * 
 * ListMailsCommand
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class ListMailsCommand extends BotCommand {
    public ListMailsCommand() {
        super("!mails");
    }

    @Override
    public void execute(PircBotX bot, Channel channel, User user, String message) {
        if (!message.contains(" ")) {
            Map<Long, Mail> mails = Mails.get(user.getNick());
            if (mails != null && mails.size() > 0) {
                ArrayList<Long> dates = new ArrayList<Long>(mails.keySet());
                Collections.sort(dates);
                for (long date : dates) {
                    Mail mail = mails.get(date);
                    SimpleDateFormat format = new SimpleDateFormat(
                            "dd-MM-YY HH:mm:ss");
                    String formDate = format.format(new Date(date));
                    bot.sendMessage(user,
                            "[" + formDate + "] " + mail.getSender() + "> "
                                    + mail.getMessage());
                }
            } else {
                bot.sendMessage(user,
                        "Entschuldige, du hast aber keine neuen Nachrichten.");
            }
        } else {
            String arg = message.substring(1);
            if (arg.equals("clear")) {
                Mails.clear(user.getNick());
                bot.sendMessage(user, "All deine Nachrichten wurden gel�scht.");
            }
        }
    }

    @Override
    public String getDisplayName() {
        return "Handle Mails";
    }
    
    @Override
    public String getUsage() {
        return "!mails [clear]";
    }
    
    @Override
    public String getHelpText() {
        return "List and clear mails you've got.";
    }
}

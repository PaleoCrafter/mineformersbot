package de.paleocrafter.mfbot.commands.pub;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;

import de.paleocrafter.mfbot.api.commands.BotCommand;

/**
 * 
 * MineFormersBot
 * 
 * TimeCommand
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class TimeCommand extends BotCommand {

    public TimeCommand() {
        super("!time");
    }

    @Override
    public void execute(PircBotX bot, Channel channel, User user, String message) {
        // Get the current calender
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("HH:mm:ss 'Uhr'");
        // Respond to the request with formatted time
        bot.sendMessage(channel, user,
                "Momentan ist es " + formater.format(cal.getTime()));
    }
    
    @Override
    public String getDisplayName() {
        return "Time";
    }
    
    public String getHelpText() {
        return "Get the current time.";
    }

}

package de.paleocrafter.mfbot.commands.pub;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;

import de.paleocrafter.mfbot.api.commands.BotCommand;

/**
 * 
 * MineFormersBot
 * 
 * GoogleCommand
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class GoogleCommand extends BotCommand {
    public GoogleCommand() {
        super("!g ");
    }

    @Override
    public void execute(PircBotX bot, Channel channel, User user, String message) {
        JSONParser parser = new JSONParser();
        URL url;
        try {
            url = new URL(
                    "https://ajax.googleapis.com/ajax/services/search/web?v=1.0&"
                            + "q=" + URLEncoder.encode(message, "UTF-8")
                            + "&userip="
                            + InetAddress.getLocalHost().getHostAddress()
                            + "&hl=de-de");
            URLConnection connection = url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            JSONObject o = (JSONObject) ((JSONObject) parser.parse(reader))
                    .get("responseData");
            JSONObject result = (JSONObject) ((JSONArray) o.get("results"))
                    .get(0);
            bot.sendMessage(channel, user, "Dein Google-Ergebnis:");
            bot.sendMessage(
                    channel,
                    Jsoup.parse((String) result.get("titleNoFormatting"))
                            .text()
                            + " ("
                            + Jsoup.parse((String) result.get("url")).text()
                            + ")");
            bot.sendMessage(channel, Jsoup
                    .parse((String) result.get("content")).text());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getDisplayName() {
        return "Google";
    }

    @Override
    public String getUsage() {
        return "!g <query>";
    }

    @Override
    public String getHelpText() {
        return "Google something without the need of your browser being started!";
    }
}

package de.paleocrafter.mfbot.commands.pub;

import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;

import de.paleocrafter.mfbot.api.commands.BotCommand;
import de.paleocrafter.mfbot.api.data.Links;

/**
 *
 * MineFormersBot
 *
 * LookupLinkCommand
 *
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 *
 */
public class LookupLinkCommand extends BotCommand {
    public LookupLinkCommand() {
        super("?");
    }

    @Override
    public void execute(PircBotX bot, Channel channel, User user, String message) {
        String sender = user.getNick();
        String key = message;
        if(message.contains(" ")) {
            String[] params = message.split(" ");
            sender = params[1];
            key = params[0];
        }
        if (Links.get(key) != null) {
            bot.sendMessage(channel, sender + ": " + Links.get(key));
        }
    }
    
    @Override
    public String getDisplayName() {
        return "Lookup Link";
    }
    
    @Override
    public String getUsage() {
        return "?<name of the link>";
    }
    
    @Override
    public String getHelpText() {
        return "Lookup the link/information under the given name.";
    }
}


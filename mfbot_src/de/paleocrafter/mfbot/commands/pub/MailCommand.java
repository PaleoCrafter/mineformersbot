package de.paleocrafter.mfbot.commands.pub;

import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;

import de.paleocrafter.mfbot.api.commands.BotCommand;
import de.paleocrafter.mfbot.api.data.Mail;
import de.paleocrafter.mfbot.api.data.Mails;

/**
 *
 * MineFormersBot
 *
 * MailCommand
 *
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 *
 */
public class MailCommand extends BotCommand {
    public MailCommand() {
        super("!mail ");
    }

    @Override
    public void execute(PircBotX bot, Channel channel, User user, String message) {
        String[] params = message.split(" ", 2);
        String name = params[0];
        String mail = params[1];
        
        Mails.add(name, System.currentTimeMillis(), new Mail(
                user.getNick(), mail));
        bot.sendMessage(user, "Deine Nachricht wurde erfolgreich verschickt.");
    }
    
    @Override
    public String getDisplayName() {
        return "Mail";
    }
    
    @Override
    public String getUsage() {
        return "!mail <recipient> <message>";
    }
    
    @Override
    public String getHelpText() {
        return "Write messages to your friends without the need of them being online!";
    }
}

package de.paleocrafter.mfbot.commands.pub;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;

import com.google.common.collect.HashBiMap;

import de.paleocrafter.mfbot.api.commands.BotCommand;
import de.paleocrafter.mfbot.api.util.BotLogger;

/**
 * 
 * MineFormersBot
 * 
 * SaRCommand
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class SaRCommand extends BotCommand {
    public SaRCommand() {
        super("!s/");
    }

    @Override
    public void execute(PircBotX bot, Channel channel, User user, String message) {
        String params[] = message.split("(?<!\\\\)/", 2);
        if (params.length == 2) {
            for (int i = 0; i < params.length; i++) {
                if (params[i].contains("\\/")) {
                    params[i] = params[i].replace("\\/", "/");
                }
            }
            ArrayList<Long> list = new ArrayList<Long>(
                    BotLogger.internalLog.columnKeySet());
            Collections.sort(list);
            Collections.reverse(list);
            boolean sentMessage = false;
            for (long time : list) {
                HashBiMap<User, String> messages = HashBiMap
                        .create(BotLogger.internalLog.column(time));
                boolean hasToBreak = false;
                for (String s : messages.values()) {
                    if (s.contains(params[0]) && !s.equals(message)
                            && !s.contains("!s/")) {
                        SimpleDateFormat formater = new SimpleDateFormat(
                                "HH:mm:ss");
                        String date = formater.format(new Date(time));
                        String msgSender = messages.inverse().get(s).getNick();
                        String newMessage = s.replace(params[0], params[1]);
                        bot.sendMessage(channel, "[" + date + "] " + msgSender
                                + "> " + newMessage);
                        hasToBreak = true;
                        sentMessage = true;
                        break;
                    }
                }
                if (hasToBreak)
                    break;
            }
            if (!sentMessage) {
                bot.sendMessage(channel, user, "Sorry, I couldn't find that.");
            }
        }
    }

    @Override
    public String getDisplayName() {
        return "Search & Replace";
    }

    @Override
    public String getUsage() {
        return "!s/<search>/<replace>";
    }

    @Override
    public String getHelpText() {
        return "Search & replace a string within the latest "
                + "message containing the searched string. Correct your friends!";
    }
}

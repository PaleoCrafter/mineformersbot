package de.paleocrafter.mfbot.commands.priv;

import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;

import de.paleocrafter.mfbot.api.commands.BotCommand;
import de.paleocrafter.mfbot.api.data.Links;

/**
 * 
 * MineFormersBot
 * 
 * LinkCommand
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class LinksCommand extends BotCommand {

    public LinksCommand() {
        super("!links ");
    }

    @Override
    public void execute(PircBotX bot, Channel channel, User user, String message) {
        String[] params = message.split(" ");
        if (params.length > 0) {
            switch (params[0]) {
                case "refresh":
                    Links.refresh();
                    bot.sendMessage(user, "Links refreshed successfully!");
                    break;
                case "add":
                    if (params.length < 3) {
                        bot.sendMessage(user,
                                "Syntax: !links add <name> <link>");
                        break;
                    } else {
                        Links.add(params[1], params[2]);
                        Links.save();
                    }
                    break;
                case "remove":
                    if (params.length < 2) {
                        bot.sendMessage(user, "Syntax: !links remove <name>");
                        break;
                    } else {
                        Links.remove(params[1]);
                        Links.save();
                    }
                    break;
                default:
                    bot.sendMessage(user,
                            "Syntax: !links <refresh|add|remove> [options]");
                    break;
            }
        } else {
            bot.sendMessage(user,
                    "Syntax: !links <refresh|add|remove> [options]");
        }
    }

    @Override
    public String getDisplayName() {
        return "Links";
    }

    @Override
    public String getUsage() {
        return "!links <refresh|add|remove> [options]";
    }

    @Override
    public String getHelpText() {
        return "Administrative tool for managing the link list.";
    }
}

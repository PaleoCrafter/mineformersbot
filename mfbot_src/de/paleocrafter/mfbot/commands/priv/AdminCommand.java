package de.paleocrafter.mfbot.commands.priv;

import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;

import de.paleocrafter.mfbot.api.commands.BotCommand;
import de.paleocrafter.mfbot.api.util.BotLogger;

/**
 * 
 * MineFormersBot
 * 
 * AdminCommand The admin command provides administration tools for the bot.
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class AdminCommand extends BotCommand {

    public AdminCommand() {
        super("!admin ");
    }

    @Override
    public void execute(PircBotX bot, Channel channel, User user, String message) {
        String[] params = message.split(" ");
        if (params.length > 0) {
            switch (params[0]) {
                case "stop":
                    BotLogger.log(null, "Going offline");
                    for (String channelName : bot.getChannelsNames()) {
                        bot.sendMessage(channelName,
                                "Der Bot verabschiedet sich herzlich von allen.");
                    }
                    BotLogger.close();
                    System.exit(0);
                    break;
                case "reconnect":
                    BotLogger.log(null, "Reconnecting...");
                    for (String channelName : bot.getChannelsNames()) {
                        bot.sendMessage(channelName,
                                "Ich bin gleich wieder da ;)");
                    }
                    bot.disconnect();
                    break;
                case "say":
                    if (params.length < 2) {
                        bot.sendMessage(user, "Syntax: !admin say <message>");
                        break;
                    }
                    for (String channelName : bot.getChannelsNames()) {
                        bot.sendMessage(channelName, params[1]);
                    }
                    break;
                case "action":
                    if (params.length < 2) {
                        bot.sendMessage(user, "Syntax: !admin action <action>");
                        break;
                    }
                    for (String channelName : bot.getChannelsNames()) {
                        bot.sendAction(channelName, params[1]);
                    }
                    break;
                default:
                    bot.sendMessage(user, getSyntaxMessage());
                    break;
            }
        } else {
            bot.sendMessage(user, getSyntaxMessage());
        }
    }

    @Override
    public String getDisplayName() {
        return "Administration";
    }

    @Override
    public String getUsage() {
        return "!admin <stop|reconnect|say|action> [options]";
    }

    @Override
    public String getHelpText() {
        return "For administration of the bot. OPs only.";
    }
}

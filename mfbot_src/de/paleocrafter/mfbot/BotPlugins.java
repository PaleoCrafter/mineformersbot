package de.paleocrafter.mfbot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;
import org.reflections.Reflections;

import de.paleocrafter.mfbot.api.plugin.BotPlugin;

/**
 * 
 * MineFormersBot
 * 
 * BotPlugins
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class BotPlugins {
    private static HashMap<String, BotPlugin> plugins;

    public static void init() {
        plugins = new HashMap<String, BotPlugin>();
    }
    
    public static Collection<BotPlugin> getPlugins() {
        return plugins.values();
    }

    public static void load(String folderPath) {
        File folder = new File(folderPath);
        if (folder.isDirectory()) {
            File[] plugins = folder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".jar") || name.endsWith(".zip");
                }
            });

            for (File file : plugins)
                loadPlugin(file);
        }
    }

    private static void loadPlugin(File file) {
        try {
            Reflections reflect = new Reflections("", new Scanner(
                    new FileInputStream(file)));
            Set<Class<? extends BotPlugin>> pluginClasses = reflect
                    .getSubTypesOf(BotPlugin.class);
            for (Class<? extends BotPlugin> pluginClass : pluginClasses) {
                BotPlugin plugin = pluginClass.newInstance();
                plugin.init();
                plugins.put(plugin.getName(), plugin);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}

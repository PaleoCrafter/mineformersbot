package de.paleocrafter.mfbot.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.EventQueue;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;

import javax.swing.JTabbedPane;
import java.awt.Insets;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.io.IOException;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JTextField;

import org.pircbotx.PircBotX;
import org.pircbotx.exception.IrcException;
import org.pircbotx.exception.NickAlreadyInUseException;
import org.pircbotx.hooks.ListenerAdapter;

import de.paleocrafter.mfbot.BotPlugins;
import de.paleocrafter.mfbot.api.configuration.Config;
import de.paleocrafter.mfbot.api.gui.ChannelPanel;
import de.paleocrafter.mfbot.api.listeners.BotListeners;
import de.paleocrafter.mfbot.api.plugin.BotPlugin;
import de.paleocrafter.mfbot.api.util.BotLogger;
import de.paleocrafter.mfbot.listeners.LogListener;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 * MineFormersBot
 * 
 * MainFrame
 * 
 * @author PaleoCrafter
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class MainFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private PircBotX bot;
    private JPanel contentPane;
    private JTextField textField;
    private JTabbedPane channelTabs;

    /**
     * Create the frame.
     */
    public MainFrame() {
        setTitle("UltraBot v1.0");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 770, 460);
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
        this.setLocation(x, y);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[] { 0, 0, 0 };
        gbl_contentPane.rowHeights = new int[] { 0, 0 };
        gbl_contentPane.columnWeights = new double[] { 0.0, 1.0,
                Double.MIN_VALUE };
        gbl_contentPane.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
        contentPane.setLayout(gbl_contentPane);

        JPanel controlPanel = new JPanel();
        GridBagConstraints gbc_controlPanel = new GridBagConstraints();
        gbc_controlPanel.fill = GridBagConstraints.VERTICAL;
        gbc_controlPanel.anchor = GridBagConstraints.WEST;
        gbc_controlPanel.insets = new Insets(0, 0, 0, 5);
        gbc_controlPanel.gridx = 0;
        gbc_controlPanel.gridy = 0;
        contentPane.add(controlPanel, gbc_controlPanel);
        GridBagLayout gbl_controlPanel = new GridBagLayout();
        gbl_controlPanel.columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
        gbl_controlPanel.rowHeights = new int[] { 0, 0, 0, 0, 0 };
        gbl_controlPanel.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0,
                1.0, Double.MIN_VALUE };
        gbl_controlPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0,
                Double.MIN_VALUE };
        controlPanel.setLayout(gbl_controlPanel);

        JPanel btnPanel = new JPanel();
        GridBagConstraints gbc_btnPanel = new GridBagConstraints();
        gbc_btnPanel.gridwidth = 3;
        gbc_btnPanel.insets = new Insets(0, 0, 5, 5);
        gbc_btnPanel.fill = GridBagConstraints.BOTH;
        gbc_btnPanel.gridx = 0;
        gbc_btnPanel.gridy = 0;
        controlPanel.add(btnPanel, gbc_btnPanel);

        JButton btnStart = new JButton("Start");
        btnStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    init();
                    bot.connect(Config.server, Config.port);
                    if (Config.shouldIdentify)
                        bot.identify(Config.password);
                    for (String channel : Config.channels)
                        bot.joinChannel(channel);
                    BotLogger.log(null, "The bot is started.");
                } catch (NickAlreadyInUseException ex) {
                    ex.printStackTrace();
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (IrcException ex) {
                    ex.printStackTrace();
                }
            }
        });
        btnPanel.add(btnStart);

        JButton btnStop = new JButton("Stop");
        btnStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BotLogger.log(null, "Going offline");
                for (String channel : bot.getChannelsNames()) {
                    bot.sendRawLineNow("/msg " + channel + " "
                            + "Der Bot verabschiedet sich herzlich von allen.");
                }
                BotLogger.close();
                LogListener.firstConnection = false;
                bot.quitServer();
            }
        });
        btnPanel.add(btnStop);

        JButton btnReconnect = new JButton("Reconnect");
        btnReconnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BotLogger.log(null, "Reconnecting...");
                for (String channelName : bot.getChannelsNames()) {
                    bot.sendMessage(channelName, "Ich bin gleich wieder da ;)");
                }
                bot.disconnect();
            }
        });
        btnPanel.add(btnReconnect);

        JLabel lblJoinChannel = new JLabel("Join Channel:");
        GridBagConstraints gbc_lblJoinChannel = new GridBagConstraints();
        gbc_lblJoinChannel.anchor = GridBagConstraints.WEST;
        gbc_lblJoinChannel.insets = new Insets(0, 0, 5, 5);
        gbc_lblJoinChannel.gridx = 0;
        gbc_lblJoinChannel.gridy = 1;
        controlPanel.add(lblJoinChannel, gbc_lblJoinChannel);

        textField = new JTextField();
        GridBagConstraints gbc_textField = new GridBagConstraints();
        gbc_textField.insets = new Insets(0, 0, 5, 5);
        gbc_textField.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField.gridx = 1;
        gbc_textField.gridy = 1;
        controlPanel.add(textField, gbc_textField);
        textField.setColumns(10);

        JButton btnJoin = new JButton("Join");
        GridBagConstraints gbc_btnJoin = new GridBagConstraints();
        gbc_btnJoin.anchor = GridBagConstraints.WEST;
        gbc_btnJoin.insets = new Insets(0, 0, 5, 5);
        gbc_btnJoin.gridx = 2;
        gbc_btnJoin.gridy = 1;
        controlPanel.add(btnJoin, gbc_btnJoin);

        channelTabs = new JTabbedPane(JTabbedPane.TOP);
        GridBagConstraints gbc_channelTabs = new GridBagConstraints();
        gbc_channelTabs.fill = GridBagConstraints.BOTH;
        gbc_channelTabs.gridx = 1;
        gbc_channelTabs.gridy = 0;
        contentPane.add(channelTabs, gbc_channelTabs);

        ChannelPanel startTab = new ChannelPanel();
        channelTabs.addTab("Global", null, startTab, null);

        BotLogger.init(Config.botName, this);
        Collection<BotPlugin> plugins = BotPlugins.getPlugins();
        for (BotPlugin plug : plugins) {
            plug.postInit();
        }
    }

    @SuppressWarnings("rawtypes")
    public void init() {
        bot = new PircBotX();
        for (ListenerAdapter listener : BotListeners.getListeners())
            bot.getListenerManager().addListener(listener);
        bot.setVerbose(false);
        bot.setName(Config.botName);
        bot.setLogin(Config.botName);
    }

    public void addToChannelLog(String channel, final String message) {
        if (channel == null) {
            final ChannelPanel globalPanel = (ChannelPanel) channelTabs
                    .getComponentAt(0);
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    globalPanel.addToLog(message);
                }
            });
            return;
        }
        int index = -1;
        for (int i = 0; i < channelTabs.getTabCount(); i++) {
            String title = channelTabs.getTitleAt(i);
            if (title.equals(channel)) {
                index = i;
                break;
            }
        }
        final ChannelPanel tab;
        if (index == -1) {
            tab = new ChannelPanel();
            channelTabs.addTab(channel, tab);
        } else {
            tab = (ChannelPanel) channelTabs.getComponentAt(index);
        }
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                tab.addToLog(message);
            }
        });
    }
}
